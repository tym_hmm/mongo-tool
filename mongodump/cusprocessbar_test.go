package mongodump

import (
	"fmt"
	"gitee.com/tym_hmm/mongo-tool/common/log"
	"gitee.com/tym_hmm/mongo-tool/common/signals"
	"gitee.com/tym_hmm/mongo-tool/common/util"
	"os"
	"testing"
	"time"
)

const (
	progressBarLength   = 24
	progressBarWaitTime = time.Second * 3
)

var (
	VersionStr = "built-without-version-string"
	GitCommit  = "build-without-git-commit"
)


func TestCusProcessBar(t *testing.T)  {
	args := []string{}
	args = append(args, "mongodb://192.168.1.169:27017")
	opts, err := ParseOptions(args, VersionStr, GitCommit)
	if err != nil {
		log.Logvf(log.Always, "error parsing command line options: %s", err.Error())
		log.Logvf(log.Always, util.ShortUsage("mongodump"))
		os.Exit(util.ExitFailure)
	}
	opts.Out = "backUp"
	opts.DB = "dataCenter"
	opts.Collection = "api_request_log_2021_09_30"
	// print help, if specified
	if opts.PrintHelp(false) {
		return
	}

	// print version, if specified
	if opts.PrintVersion() {
		return
	}

	// init logger
	log.SetVerbosity(opts.Verbosity)

	// verify uri options and log them
	opts.URI.LogUnsupportedOptions()

	// kick off the progress bar manager
	//progressManager := progress.NewBarWriter(log.Writer(0), progressBarWaitTime, progressBarLength, false)
	//progressManager.Start()
	//defer progressManager.Stop()

	progressManager := NewCusBarWriter(log.Writer(0), progressBarWaitTime, progressBarLength, false, NewProcessListener())
	progressManager.Start()
	defer progressManager.Stop()

	dump := MongoDump{
		ToolOptions:     opts.ToolOptions,
		OutputOptions:   opts.OutputOptions,
		InputOptions:    opts.InputOptions,
		ProgressManager: progressManager,
	}

	finishedChan := signals.HandleWithInterrupt(dump.HandleInterrupt)
	defer close(finishedChan)

	if err = dump.Init(); err != nil {
		log.Logvf(log.Always, "Failed: %v", err)
		os.Exit(util.ExitFailure)
	}
	if err = dump.Dump(); err != nil {
		log.Logvf(log.Always, "Failed: %v", err)
		os.Exit(util.ExitFailure)
	}
}


type processListener struct {

}

func NewProcessListener() *processListener {
	return &processListener{}
}

func (p *processListener) Start(name string) {
	fmt.Println("start", name)
}

func (p *processListener) Pending(name string, percent float64) {
	fmt.Println("Pending", name, percent)
}

func (p *processListener) Complete(name string) {
	fmt.Println("Complete", name)
}

func (p *processListener) Error(message string) {
	fmt.Println("Error", message)
}

func (p *processListener) TaskError(name string, message string) {
	fmt.Println("TaskError", name, message)
}
